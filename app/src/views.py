import json

from aiohttp.web import Response, json_response

from utils import json_response_asf, error_response

from anogrammer import get_anagram_key, validate_words, validate_single_word
from data_types import LoadResult, ListResult, BoolResult


async def index(request):
    return json_response_asf('I\'m anagram checker')


async def load_words(request):
    redis = request.app['redis']
    try:
        data = await request.json()
    except json.decoder.JSONDecodeError:
        data = None

    if data:
        tr = redis.multi_exec()
        validated, wrong = await validate_words(data)
        if validated:
            for word in validated:
                key = await get_anagram_key(word)
                tr.set(f'word:{word}', key)
                tr.sadd(f'key:{key}', word)
            await tr.execute()

            result: LoadResult = {
                'result': {
                    'status': 'OK',
                    'get': len(data),
                    'add': len(validated),
                    'wrong': len(wrong)
                }
            }
            return json_response(result)
        else:
            return error_response('All the words is bad, please pass correct words')

    else:
        return error_response('Pls pass some words in json array')


async def get_anagrams(request) -> Response:
    redis = request.app['redis']
    word = request.query.get('word', '')

    if await validate_single_word(word):
        key = await redis.get(f'word:{word}', encoding='utf-8')
        if key:
            palindroms = await redis.smembers(f'key:{key}', encoding='utf-8')
        else:
            key = await get_anagram_key(word)
            await redis.set(f'word:{word}', key)
            await redis.sadd(f'key:{key}', word)
            palindroms = await redis.smembers(f'key:{key}', encoding='utf-8')

        result: ListResult = {'result': palindroms}

        return json_response_asf(result)
    else:
        return error_response('Pls pass word in query params: /get?word=foobar')


async def is_anagrams(request):
    redis = request.app['redis']
    first = request.query.get('first', '')
    second = request.query.get('second', '')

    words = [first, second]
    validated, wrong = await validate_words(words)

    if not wrong and len(validated) == 2:
        keys = []
        for word in validated:
            # добавляем в нашу базу
            key = await get_anagram_key(word)
            keys.append(key)
            exists = await redis.get(f'word:{word}')
            if not exists:
                await redis.set(f'word:{word}', key)
                await redis.sadd(f'key:{key}', word)

        response: BoolResult = {'result': True}
        if keys[0] != keys[1]:
            response['result'] = False
        return json_response_asf(response)

    else:
        return error_response('Please pass two correct words: /is_anagramm?first=foo&second=oof')
