import string
from typing import List, Tuple

ABRACADABRA = [*list(string.printable)[0:10], *list(string.printable)[62:]]


async def validate_single_word(word: str) -> bool:
    if len(word):
        for letter in word:
            if letter in ABRACADABRA:
                return False
        else:
            return True
    else:
        return False

async def validate_words(words: List[str]) -> Tuple[List[str], List[str]]:
    words_set = set(words)
    checked_words = []
    wrong_words = []

    for word in words_set:
        prepeared = await prepare_word(word)
        if await validate_single_word(prepeared):
            checked_words.append(prepeared)
        else:
            wrong_words.append(prepeared)

    return (checked_words, wrong_words)


async def prepare_word(word: str) -> str:
    lower_word = word.lower()
    striped_word = lower_word.strip()
    return striped_word


async def get_anagram_key(word: str) -> str:
    prepared_word = await prepare_word(word)
    return ''.join(sorted(prepared_word))
