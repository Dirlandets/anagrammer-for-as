import os

try:
    DEBUG = os.environ.get('ENV') not in ['PROD', ]
except KeyError:
    DEBUG = True

redis_url = os.environ.get('REDIS_URL', 'localhost')

REDIS_HOST = f'redis://{redis_url}:6379/2'
REDIS_TEST_HOST = f'redis://{redis_url}:6379/3'
