from views import index, load_words, get_anagrams, is_anagrams


def setup_routes(app):
    app.router.add_get('', index)
    app.router.add_post('/load', load_words)
    app.router.add_get('/get', get_anagrams)
    app.router.add_get('/is_anagrams', is_anagrams)
