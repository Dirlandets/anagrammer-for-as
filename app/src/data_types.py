from typing import TypedDict, List


class Result(TypedDict):
    status: str
    get: int
    add: int
    wrong: int

class LoadResult(TypedDict):
    result: Result


class ListResult(TypedDict):
    result: List[str]


class BoolResult(TypedDict):
    result: bool
