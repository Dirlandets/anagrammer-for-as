#!/bin/bash
set -e

if [ "$ENV" = 'PROD' ]; then
    echo "PRODUCTION" 
    exec gunicorn main:get_app --bind 0.0.0.0:8080 --worker-class aiohttp.GunicornWebWorker
else
    echo "DEVELOP"
    exec python main.py -P 8080
fi