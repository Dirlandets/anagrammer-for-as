import unittest
from .helpers import with_loop

from anogrammer import (
    prepare_word, get_anagram_key, validate_words,
)

TRUE_EXAMPLES = [
    ['aviasales', 'siaalveas'],
    ['aviasales', 'aviasales'],
    ['Aviasales', 'siaaLveas'],
    ['AviaSales', 'SIAALVEAS'],
    ['AviaSales ', 'SIAALVEAS'],
    ['barfoo', 'foobar']
]

class TestCreateAnagrammKey(unittest.TestCase):
    @with_loop
    async def test_can_make_keys(self):
        word = 'cab'
        key = await get_anagram_key(word)
        self.assertEqual(key, 'abc')

    @with_loop
    async def test_compare_two_keys_true(self):
        for pair in TRUE_EXAMPLES:
            key_1 = await get_anagram_key(pair[0])
            key_2 = await get_anagram_key(pair[1])
            self.assertEqual(key_1, key_2)

    @with_loop
    async def test_compare_two_counters_false(self):
        word_1 = 'aviasales'
        word_2 = 'saymeword'
        key_1 = await get_anagram_key(word_1)
        key_2 = await get_anagram_key(word_2)
        with self.assertRaises(AssertionError):
            self.assertEqual(key_1, key_2)


class TestPrepareWord(unittest.TestCase):
    @with_loop
    async def test_return_same_result(self):
        words = ['life', 'Life', 'LIFE ', ' life']
        prepared_words = []
        for word in words:
            prepared_words.append(await prepare_word(word))

        self.assertTrue(all([word == 'life' for word in prepared_words]))


class TestValidator(unittest.TestCase):
    @with_loop
    async def test_validator_can_validate_if_corect(self):
        words = ['life', 'Life', 'LIFE ', ' life']
        validated, wrong = await validate_words(words)
        self.assertEqual(len(validated), len(words))
        self.assertEqual(len(wrong), 0)

    @with_loop
    async def test_validator_can_validate_if_wrong(self):
        words = ['li fe', 'Li#fe', 'LI*FE ', ' ', '']
        validated, wrong = await validate_words(words)
        self.assertEqual(len(wrong), len(wrong))
        self.assertEqual(len(validated), 0)

    @with_loop
    async def test_validator_can_validate_if_mix(self):
        incorrect = ['li fe', 'Li#fe', 'LI*FE ', ' ', '']
        correct = ['life', 'Life', 'LIFE ', ' life']
        words = [*incorrect, *correct]
        validated, wrong = await validate_words(words)
        self.assertEqual(len(wrong), len(incorrect))
        self.assertEqual(len(validated), len(correct))


if __name__ == "__main__":
    unittest.main()
