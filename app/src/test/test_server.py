import unittest
import json
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from main import get_app
from data_types import LoadResult, ListResult, BoolResult


class HealthCheck(AioHTTPTestCase):

    async def get_application(self):
        app = await get_app(testing=True)
        return app

    @unittest_run_loop
    async def test_its_alive(self):
        resp = await self.client.request("GET", "/")
        assert resp.status == 200
        text = await resp.text()
        assert "I'm anagram checker" in text


class TestLoadEndpoint(AioHTTPTestCase):

    async def get_application(self):
        app = await get_app(testing=True)
        return app

    @unittest_run_loop
    async def test_can_all_is_correct(self):
        data = ["foobar", "aabb", "baba", "boofar", "test"]
        expected_response: LoadResult = {
            'result': {
                "status": "OK",
                "get": len(data),
                "add": len(data),
                "wrong": 0
            }
        }

        data_json = json.dumps(data)
        resp = await self.client.request('POST', '/load', data=data_json)
        self.assertEqual(resp.status, 200)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)

    @unittest_run_loop
    async def test_can_all_is_correct_and_wrong(self):
        correct = ["foobar", "aabb", "baba", "boofar", "test"]
        wrong = ["f1oobar", "a abb", "b*aba", "bo$ofar", " ", ""]

        words = [*correct, *wrong]
        expected_response: LoadResult = {
            'result': {
                "status": "OK",
                "get": len(words),
                "add": len(correct),
                "wrong": len(wrong)
            }
        }
        data_json = json.dumps(words)
        resp = await self.client.request('POST', '/load', data=data_json)
        self.assertEqual(resp.status, 200)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)

    @unittest_run_loop
    async def test_can_all_is_wrong(self):
        data = ["f1oobar", "a abb", "b*aba", "bo$ofar", " ", ""]
        expected_response = {'error': 'All the words is bad, please pass correct words'}
        data_json = json.dumps(data)
        resp = await self.client.request('POST', '/load', data=data_json)
        self.assertEqual(resp.status, 400)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)

    @unittest_run_loop
    async def test_accept_only_with_data(self):
        expected_response = {'error': 'Pls pass some words in json array'}
        resp = await self.client.request('POST', '/load', data=[])
        self.assertEqual(resp.status, 400)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)


class TestGetEndpoint(AioHTTPTestCase):
    async def get_application(self):
        app = await get_app(testing=True)
        return app

    @unittest_run_loop
    async def test_have_to_pass_some_word(self):
        expected_response = {'error': 'Pls pass word in query params: /get?word=foobar'}
        resp = await self.client.request('GET', '/get')
        self.assertEqual(resp.status, 400)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)

    @unittest_run_loop
    async def test_return_preseted_data(self):
        correct = ["foobar", "aabb", "baba", "boofar", "test"]
        data_json = json.dumps(correct)
        resp = await self.client.request('POST', '/load', data=data_json)

        resp = await self.client.request('GET', '/get?word=barfoo')
        expected_response: ListResult = {
            'result': [
                'foobar',
                'boofar',
                'barfoo'
            ]
        }
        self.assertEqual(resp.status, 200)
        response_data = await resp.json()
        self.assertEqual(len(expected_response['result']), len(response_data['result']))
        for word in expected_response['result']:
            self.assertIn(word, response_data['result'])

    @unittest_run_loop
    async def test_can_add_anagrams_when_user_searching(self):
        resp_1 = await self.client.request('GET', '/get?word=barfoo')
        resp_2 = await self.client.request('GET', '/get?word=foobar')
        self.assertEqual(resp_1.status, 200)
        self.assertEqual(resp_2.status, 200)

        response_data_1 = await resp_1.json()
        response_data_2 = await resp_2.json()

        expected_response_1: ListResult = {
            'result': [
                'barfoo'
            ]
        }

        expected_response_2: ListResult = {
            'result': [
                'foobar',
                'barfoo',
            ]
        }

        self.assertEqual(response_data_1, expected_response_1)
        for word in response_data_2['result']:
            self.assertIn(word, expected_response_2['result'])


class TestIsAnogrammEndpoint(AioHTTPTestCase):
    async def get_application(self):
        app = await get_app(testing=True)
        return app

    @unittest_run_loop
    async def test_can_compare(self):
        expected_response: BoolResult = {'result': True}
        resp = await self.client.request('GET', '/is_anagrams?first=foo&second=oof')
        self.assertEqual(resp.status, 200)
        response_data = await resp.json()
        self.assertEqual(response_data, expected_response)

    @unittest_run_loop
    async def test_compared_words_add_to_db(self):
        # Step 1: compare
        resp_1 = await self.client.request('GET', '/is_anagrams?first=foobar&second=barfoo')
        # Step 2: check in db
        resp_2 = await self.client.request('GET', '/get?word=foobar')

        expected_response_1 = {
            'result': [
                'foobar',
                'barfoo',
            ]
        }
        self.assertEqual(resp_1.status, 200)
        self.assertEqual(resp_2.status, 200)

        response_data_1 = await resp_1.json()
        response_data_2 = await resp_2.json()

        self.assertTrue(response_data_1['result'])
        for word in response_data_2['result']:
            self.assertIn(word, expected_response_1['result'])


if __name__ == "__main__":
    unittest.main()
