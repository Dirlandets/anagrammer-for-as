import json
from typing import Any
from aiohttp.web import Response

def json_response_asf(data: Any, code: int = 200, ensure_ascii=False) -> Response:
    response = Response(status=code)
    response.content_type = 'application/json'
    response.text = json.dumps(data, ensure_ascii=ensure_ascii)
    return response


def error_response(text: str, code: int = 400) -> Response:
    return json_response_asf({'error': text}, code)
