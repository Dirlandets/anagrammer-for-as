# Тестовое задание для AS

## Эндпоинты:
1. Загрузить словарь POST http://127.0.0.1:8080/load '["foobar", "aabb", "baba", "boofar", "test"]' => result: {
            status: 'OK',
            get: len(data),
            add: len(validated),
            wrong: len(wrong)
        }
2. Поиск GET http://127.0.0.1:8080/get?word=foobar' => 'result': ["foobar","boofar"]
3. Узнать являются ли слова анаграммами:  
    http://127.0.0.1:8080/is_anagrams?first=foobar&second=aabb = False
    http://127.0.0.1:8080/is_anagrams?first=foobar&second=boofar = True


## Запуск
### Сервер для разработки(с автоперезагрузкой): 
docker-compose -f docker-compose-dev.yml up --build

### Или без докера 
(нужен работающий инстанс редиса на localhost:6379):
python app/src/main.py -P 8080 

### Продакашн сервер: 
docker-compose up --build

## Тесты
docker-compose exec -T backend python -m unittest discover .

docker-compose exec -T backend python -m mypy --config-file ../mypy.ini .

docker-compose exec -T backend python -m flake8 .



## Coverage
docker-compose exec -T backend coverage run --omit="*/test*" -m unittest discover .

docker-compose exec -T backend coverage report -m
